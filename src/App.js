
import { Container } from '@mui/material';
import './App.css';

import HeaderComponent from './components/header/headerComponent ';
import FooterComponent from './components/footer/footerComponent';
import ContentComponent from './components/content/contentComponent';
function App() {
  return (
    <Container>

      {/* <!-- Header --> */}
      <HeaderComponent></HeaderComponent>

      {/* <!-- Content --> */}
      <ContentComponent></ContentComponent>

      {/* <!-- Footer --> */}
      <FooterComponent></FooterComponent>

    </Container>
  );
}

export default App;
