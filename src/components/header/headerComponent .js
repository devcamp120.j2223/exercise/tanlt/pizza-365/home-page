import {AppBar, Toolbar, Button, Grid } from '@mui/material';

function HeaderComponent() {
    return (
        <AppBar component="nav">
            <Toolbar className="bg-orange">
                <Grid sx={{ width:"100%"}} textAlign="center">
                    <Grid item sx={{display: "inline"}} paddingRight={15} paddingLeft={15}>
                        <Button sx={{ color: "black" }}>
                            Trang chủ
                        </Button>
                    </Grid>
                    <Grid item sx={{display: "inline"}} paddingRight={15} paddingLeft={15}>
                        <Button sx={{ color: "black" }}>
                            Combo
                        </Button>
                    </Grid>
                    <Grid item sx={{display: "inline"}} paddingRight={15} paddingLeft={15}>
                        <Button sx={{ color: "black" }}>
                            Loại pizza
                        </Button>
                    </Grid>
                    <Grid item sx={{display: "inline"}} paddingRight={15} paddingLeft={15}>
                        <Button sx={{ color: "black" }}>
                            Gửi đơn hàng
                        </Button>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    )
}
export default HeaderComponent;