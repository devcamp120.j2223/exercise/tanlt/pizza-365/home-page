import { Card, List, ListItemText, ListItem, Grid, Typography, CardContent, Button, CardActions, CardHeader } from '@mui/material';
import { useEffect, useState } from 'react';

function SizeComponent({ sizeProps }) {

    const [size, setSize] = useState({})

    const btnSmallClick = () => {
        setSize({
            menuName: "S",    // S, M, L
            duongKinhCM: "20",
            suonNuong: 2,
            saladGr: 200,
            drink: 2,
            priceVND: 150000
        })
    }

    const btnMediumClick = () => {
        setSize({
            menuName: "M",    // S, M, L
            duongKinhCM: "25",
            suonNuong: 4,
            saladGr: 300,
            drink: 3,
            priceVND: 200000
        })
    }

    const btnLargeClick = () => {
        setSize({
            menuName: "L",    // S, M, L
            duongKinhCM: "30",
            suonNuong: 8,
            saladGr: 500,
            drink: 4,
            priceVND: 250000
        })
    }

    useEffect(() => {
    sizeProps(size)
    }, [size])
    return (
        <Grid id="plans">
            {/* <!-- Title Menu pizza --> */}
            <Grid sm={12} textAlign="center" p={4} mt={4} className="text-orange">
                <Typography variant='h3' sx={{ borderBottom: 1 }}>Chọn size pizza</Typography>
                <Typography p={2}>Chọn combo pizza phù hợp với nhu cầu của bạn.</Typography>
            </Grid>
            {/* <!-- Content Menu pizza --> */}
            <Grid sm={12}>
                <Grid container>

                    <Grid sm={4} textAlign="center">
                        <Card sx={{ maxWidth: 345, border: 1, borderColor: "#dfdfdf" }}>
                            <CardHeader title="S (small)" className="bg-orange" />
                            <CardContent sx={{ borderBottom: 1, borderColor: "#dfdfdf" }}>
                                <List>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Đường kính:&nbsp;<b>20cm</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Sườn nướng:&nbsp;<b>2</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Salad:&nbsp;<b>200g</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Nước ngọt:&nbsp;<b>2</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText><Typography variant='h3'>150.000</Typography></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>VNĐ</ListItemText>
                                    </ListItem>
                                </List>
                            </CardContent>
                            <CardActions>
                                <Button variant='contained' sx={{ backgroundColor: "orange", color: "black", width: "100%" }} id="btn-small" onClick={btnSmallClick}>Chọn</Button>
                            </CardActions>
                        </Card>
                    </Grid>

                    <Grid sm={4} textAlign="center">
                        <Card sx={{ maxWidth: 345, border: 1, borderColor: "#dfdfdf" }}>
                            <CardHeader title="M (medium)" className="bg-orange" />
                            <CardContent sx={{ borderBottom: 1, borderColor: "#dfdfdf" }}>
                                <List>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Đường kính:&nbsp;<b>25cm</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Sườn nướng:&nbsp;<b>4</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Salad:&nbsp;<b>300g</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Nước ngọt:&nbsp;<b>3</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText><Typography variant='h3'>200.000</Typography></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>VNĐ</ListItemText>
                                    </ListItem>
                                </List>
                            </CardContent>
                            <CardActions>
                                <Button variant='contained' sx={{ backgroundColor: "orange", color: "black", width: "100%" }} id="btn-medium" onClick={btnMediumClick}>Chọn</Button>
                            </CardActions>
                        </Card>
                    </Grid>

                    <Grid sm={4} textAlign="center">
                        <Card sx={{ maxWidth: 345, border: 1, borderColor: "#dfdfdf" }}>
                            <CardHeader title="L (large)" className="bg-orange" />
                            <CardContent sx={{ borderBottom: 1, borderColor: "#dfdfdf" }}>
                                <List>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Đường kính:&nbsp;<b>30cm</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Sườn nướng:&nbsp;<b>8</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Salad:&nbsp;<b>500g</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>Nước ngọt:&nbsp;<b>4</b></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText><Typography variant='h3'>250.000</Typography></ListItemText>
                                    </ListItem>
                                    <ListItem sx={{ textAlign: "center" }}>
                                        <ListItemText>VNĐ</ListItemText>
                                    </ListItem>
                                </List>
                            </CardContent>
                            <CardActions>
                                <Button variant='contained' sx={{ backgroundColor: "orange", color: "black", width: "100%" }} id="btn-large" onClick={btnLargeClick}>Chọn</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
}
export default SizeComponent;