import { Input, Grid, Typography, Button, Alert, Snackbar } from '@mui/material';
import {useEffect, useState } from 'react';
import { fetchAPI } from '../../FetchAPI';

function FormComponent({ formProps }) {
    const [fullName, setFullname] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [diachi, setDiachi] = useState("");
    const [voucher, setVoucher] = useState("");
    const [message, setMessage] = useState("");
    const [submit, setSubmit] = useState(false);
    const [checkEmail, setCheckEmail] = useState(true);
    const [checkPhone, setCheckPhone] = useState(true);
    const [giamGia, setGiamGia] = useState(0);
    const [openAlert, setOpenAlert] = useState(false);
    const handleClose = () => { setOpenAlert(false) }


    const onBtnGui = () => {
        setSubmit(true)
        kiemTraVoucher();
        if (validateData()) {
            formProps({
                hoTen: fullName,
                email: email,
                soDienThoai: phone,
                diaChi: diachi,
                loiNhan: message,
                idVourcher: voucher,
                giamGia: giamGia
            })
        }
    }
    const validateData = () => {
        if (fullName === "") {
            return false
        }
        if (email.indexOf("@") === -1) {
            setCheckEmail(false)
            return false
        } else { setCheckEmail(true) }
        if (isNaN(phone)) {
            setCheckPhone(false);
            return false
        }
        if (phone === "") {
            setCheckPhone(false);
            return false
        } else { setCheckPhone(true) }
        if (diachi === "") {
            return false
        }
        return true;
    }

    const kiemTraVoucher = () => {
        if (voucher) {
            fetchAPI("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + voucher)
                .then((data) => {
                    setGiamGia(data.phanTramGiamGia);
                })
                .catch((err) => {
                    setOpenAlert(true)
                })
        }
    }

    useEffect(() => {
        if (validateData()) {
            formProps({
                hoTen: fullName,
                email: email,
                soDienThoai: phone,
                diaChi: diachi,
                loiNhan: message,
                idVourcher: voucher,
                giamGia: giamGia
            })
        }
    }, [giamGia])
    return (
        <Grid id="contact" mb={4}>
            {/* <!-- Title Gửi đơn hàng --> */}
            <Grid sm={12} textAlign="center" p={4} mt={4} className="text-orange">
                <Typography variant='h3' sx={{ borderBottom: 1 }}>Gửi đơn hàng</Typography>
            </Grid>
            {
                openAlert ?
                    <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                           Mã giảm giá không tồn tại
                        </Alert>
                    </Snackbar> : null
            }
            {/* <!-- Content Gửi đơn hàng --> */}
            <Grid container sm={12} p={2} sx={{ borderRadius: "10px", backgroundColor: "#e9ecef" }}>

                <Grid sm={12}>
                    <Grid item>
                        <Typography htmlFor="fullname">Tên</Typography>
                        <Input sx={{ borderRadius: "5px", padding: 0.5, backgroundColor: "#ffffff" }} value={fullName} id="inp-fullname" placeholder="Nhập tên" fullWidth onChange={(event) => { setFullname(event.target.value) }} />
                        {
                            submit
                                ? (fullName ? null : <Typography color="error">Nhập Fullname</Typography>)
                                : null

                        }
                    </Grid>
                    <Grid item mt={3}>
                        <Typography htmlFor="email">Email</Typography>
                        <Input sx={{ borderRadius: "5px", padding: 0.5, backgroundColor: "#ffffff" }} value={email} id="inp-email" placeholder="Nhập email" fullWidth onChange={(event) => { setEmail(event.target.value) }} />
                        {
                            submit
                                ? (checkEmail ? null : <Typography color="error">Email không hợp lệ</Typography>)
                                : null

                        }
                    </Grid>
                    <Grid item mt={3}>
                        <Typography htmlFor="dien-thoai">Số điện thoại</Typography>
                        <Input sx={{ borderRadius: "5px", padding: 0.5, backgroundColor: "#ffffff" }} value={phone} id="inp-dien-thoai" placeholder="Nhập số điện thoại" fullWidth onChange={(event) => { setPhone(event.target.value) }} />
                        {
                            submit
                                ? (checkPhone ? null : <Typography color="error">Nhập số điện thoại</Typography>)
                                : null

                        }
                    </Grid>
                    <Grid item mt={3}>
                        <Typography htmlFor="dia-chi">Địa chỉ</Typography>
                        <Input sx={{ borderRadius: "5px", padding: 0.5, backgroundColor: "#ffffff" }} value={diachi} id="inp-dia-chi" placeholder="Địa chỉ" fullWidth onChange={(event) => { setDiachi(event.target.value) }} />
                        {
                            submit
                                ? (diachi ? null : <Typography color="error">Nhập địa chỉ</Typography>)
                                : null

                        }
                    </Grid>
                    <Grid item mt={3}>
                        <Typography htmlFor="voucher">Mã giảm giá</Typography>
                        <Input sx={{ borderRadius: "5px", padding: 0.5, backgroundColor: "#ffffff" }} value={voucher} id="inp-voucher" placeholder="Nhập mã giảm giá" fullWidth onChange={(event) => { setVoucher(event.target.value) }} />
                    </Grid>
                    <Grid item mt={3} mb={3}>
                        <Typography htmlFor="message">Lời nhắn</Typography>
                        <Input sx={{ borderRadius: "5px", padding: 0.5, backgroundColor: "#ffffff" }} value={message} id="inp-message" placeholder="Nhập lời nhắn" fullWidth onChange={(event) => { setMessage(event.target.value) }} />
                    </Grid>
                    <Button variant='contained' sx={{ backgroundColor: "orange", color: "black", width: "100%" }} id="btn-send" onClick={onBtnGui}>Gửi</Button>
                </Grid>
            </Grid>
        </Grid>
    )

}
export default FormComponent;