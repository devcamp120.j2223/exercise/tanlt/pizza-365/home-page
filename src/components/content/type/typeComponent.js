import { Card, Grid, Typography, CardContent, Button, CardActions, CardMedia } from '@mui/material';


import seafood from '../../../assets/images/seafood.jpg';
import bacon from '../../../assets/images/bacon.jpg';
import hawaiian from '../../../assets/images/hawaiian.jpg';

function TypeComponent({typeProps}) {


    const btnSeafoodClick = () => {

        typeProps("Seafood")
    }

    const btnHawaiiClick = () => {

        typeProps("Hawaii")
    }

    const btnBaconClick = () => {

        typeProps("Bacon")
    }
    return (
        <div id="about" className="row">
            {/* <!-- Title Chọn loại Pizza --> */}
            <Grid sm={12} textAlign="center" p={4} mt={4} className="text-orange">
                <Typography variant='h3' sx={{ borderBottom: 1 }}>Chọn loại Pizza</Typography>
            </Grid>
            {/* <!-- Content Chọn loại Pizza --> */}
            <Grid sm={12}>
                <Grid container>
                    <Grid sm={4}>
                        <Card sx={{ maxWidth: 370}}>
                            <CardMedia
                                component="img"
                                image={seafood}
                                alt="seafood"
                            />
                            <CardContent sx={{ padding: 4 }}>
                                <Typography gutterBottom variant="h6" component="div">
                                    OCEAN MANIA
                                </Typography>
                                <Typography variant="body1">
                                    PIZZA HẢI SẢN XỐT MYONNNAISE
                                </Typography>
                                <Typography variant="body1" sx={{ paddingTop: 2, paddingBottom: 3  }}>
                                    Xốt Cà chua, Phô mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây.
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button variant='contained' sx={{ backgroundColor: "orange", color: "black", width: "100%" }} id="btn-hai-san" onClick={btnSeafoodClick}>Chọn</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid sm={4}>
                        <Card sx={{ maxWidth: 370}}>
                            <CardMedia
                                component="img"
                                image={hawaiian}
                                alt="hawaiian"
                            />
                            <CardContent sx={{ padding: 4 }}>
                                <Typography gutterBottom variant="h6" component="div">
                                    HAWAIIAN
                                </Typography>
                                <Typography variant="body1">
                                    PIZZA DĂM BÔNG DỨA KIỂU HAWAII
                                </Typography>
                                <Typography variant="body1" sx={{ paddingTop: 2, paddingBottom: 3 }}>
                                    Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button variant='contained' sx={{ backgroundColor: "orange", color: "black", width: "100%" }} id="btn-hawai" onClick={btnHawaiiClick}>Chọn</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid sm={4}>
                        <Card sx={{ maxWidth: 370}}>
                            <CardMedia
                                component="img"
                                image={bacon}
                                alt="bacon"
                            />
                            <CardContent sx={{ padding: 4 }}>
                                <Typography gutterBottom variant="h6" component="div">
                                    CHEESY CHICKEN BACON
                                </Typography>
                                <Typography variant="body1">
                                    PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI
                                </Typography>
                                <Typography variant="body1" sx={{ paddingTop: 2 }}>
                                    Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button variant='contained' sx={{ backgroundColor: "orange", color: "black", width: "100%" }} id="btn-thit-hun-khoi" onClick={btnBaconClick}>Chọn</Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )

}
export default TypeComponent;