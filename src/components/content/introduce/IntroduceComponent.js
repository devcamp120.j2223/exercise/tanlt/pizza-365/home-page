import { Box, Container, Grid, Typography, MobileStepper, Button } from '@mui/material';
import { useState } from 'react';
import { useTheme } from '@mui/material/styles';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import img1 from '../../../assets/images/1.jpg';
import img2 from '../../../assets/images/2.jpg';
import img3 from '../../../assets/images/3.jpg';
import img4 from '../../../assets/images/4.jpg';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
const images = [
  {
    imgPath: img1
  },
  {
    imgPath: img2
  },
  {
    imgPath: img3
  },
  {
    imgPath: img4
  },
];

function IntroduceComponent() {
  const theme = useTheme();
  const [activeStep, setActiveStep] = useState(0);
  const maxSteps = images.length;
  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Container>
      {/* <!-- Title row home --> */}
      <Grid className="text-orange">
        <Grid mt={4}>
          <Typography variant='h4'>Pizza 365</Typography>
          <Typography className="p-truly-italian">Truly italian !</Typography>
        </Grid>
      </Grid>

      {/* <!-- Slide row home --> */}
      <Box sx={{ maxWidth: "100%", flexGrow: 1 }}>
        <AutoPlaySwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={activeStep}
          onChangeIndex={handleStepChange}
          enableMouseEvents
        >
          {images.map((step, index) => (
            <div key={step.label}>
              {Math.abs(activeStep - index) <= 2 ? (
                <Box
                  component="img"
                  sx={{
                    height: "100%",
                    display: 'block',
                    maxWidth: "100%",
                    overflow: 'hidden',
                    width: '100%',
                  }}
                  src={step.imgPath}
                  alt={step.label}
                />
              ) : null}
            </div>
          ))}
        </AutoPlaySwipeableViews>
        <MobileStepper
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          nextButton={
            <Button
              size="small"
              onClick={handleNext}
              disabled={activeStep === maxSteps - 1}
            >
              Next
              {theme.direction === 'rtl' ? (
                <KeyboardArrowLeft />
              ) : (
                <KeyboardArrowRight />
              )}
            </Button>
          }
          backButton={
            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? (
                <KeyboardArrowRight />
              ) : (
                <KeyboardArrowLeft />
              )}
              Back
            </Button>
          }
        />
      </Box>

      {/* <!-- Title Tại sao lại pizza 365 --> */}
      <Grid sm={12} p={4} mt={4} textAlign="center">
        <Typography variant='h4'><b className="text-orange">Tại sao lại pizza 365</b></Typography>
      </Grid>

      {/* <!-- Content Tại sao lại pizza 365 --> */}
      <Grid container>

        <Grid sm={3} p={3} className="bg-bg-lightgoldenrodyellow" sx={{ border: 1, borderColor:"orange" }}>
          <Typography variant='h5' p={2}>Đa dạng</Typography>
          <Typography p={2}>Số lượng Pizza đa dạng, có đầy đủ các loại Pizza đang hot nhất hiện nay.</Typography>
        </Grid>
        <Grid sm={3} p={3} className="bg-yellow" sx={{ border: 1, borderColor:"orange" }}>
          <Typography variant='h5' p={2}>Chất lượng</Typography>
          <Typography p={2}>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an
            toàn thực phẩm.</Typography>
        </Grid>
        <Grid sm={3} p={3} className="bg-lightsalmon" sx={{ border: 1, borderColor:"orange" }}>
          <Typography variant='h5' p={2}>Hương vị</Typography>
          <Typography p={2}>Đảm bảo hương vị ngon độc lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.
          </Typography>
        </Grid>
        <Grid sm={3} p={3} className="bg-orange" sx={{ border: 1, borderColor:"orange" }}>
          <Typography variant='h5' p={2}>Dịch vụ</Typography>
          <Typography p={2}>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng,
            tân tiến.</Typography>
        </Grid>

      </Grid>
    </Container>
  )

}
export default IntroduceComponent;