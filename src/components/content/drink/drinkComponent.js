import { Select, MenuItem, Grid, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { fetchAPI } from '../../FetchAPI';

function DrinkComponent({drinkProps}) {

    const [drink, setDrink] = useState([])

    const changeSelectDrink = (event) =>{
        drinkProps(event.target.value);
    }

    useEffect(() => {
        fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                setDrink(data);
            })
    }, [])
    return (
        <Grid>
            {/* <!-- Title Đồ Uống --> */}
            <Grid sm={12} textAlign="center" p={4} mt={4} className="text-orange">
                <Typography variant='h3' sx={{ borderBottom: 1 }}>Chọn đồ uống</Typography>
            </Grid>
            {/* <!-- Content Đồ Uống --> */}
            <Grid sm={12}>
                <Select id="select-drink" defaultValue={0} fullWidth onChange={changeSelectDrink}>
                    <MenuItem value={0}>Tất cả loại nước uống</MenuItem>
                    {
                        drink.map((drink, index)=>{
                           return <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                        })
                    }
                </Select>
            </Grid>
        </Grid>
    )
}
export default DrinkComponent;