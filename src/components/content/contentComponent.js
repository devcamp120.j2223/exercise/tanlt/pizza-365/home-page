import { Grid, Modal, Box, Typography, Button } from '@mui/material';
import { useState } from 'react';
import { fetchAPI } from '../FetchAPI';
import DrinkComponent from "./drink/drinkComponent";
import FormComponent from "./form/formComponent";
import IntroduceComponent from "./introduce/IntroduceComponent";
import SizeComponent from "./size/sizeComponent";
import TypeComponent from "./type/typeComponent";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "350px",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    p: 4,
};

function ContentComponent() {
    const [size, setSize] = useState("");
    const [typePizza, setTypePizza] = useState("");
    const [drink, setDrink] = useState("");
    const [form, setForm] = useState("");
    const [open, setOpen] = useState(false);
    const handleClose = () => { setOpen(false) }
    const [maDonHang, setMaDonHang] = useState("");
    const [openCon, setOpenCon] = useState(false);
    const handleCloseCon = () => { setOpenCon(false) }

    const getSize = (value) => {
        setSize(value);
    }

    const getType = (value) => {
        setTypePizza(value);
    }

    const getDrink = (value) => {
        setDrink(value);
    }

    const getForm = (value) => {
        setForm(value);
        setOpen(true)
    }
    const taoDonHang = () => {
        let body = {
            method: 'POST',
            body: JSON.stringify({
                kichCo: size.menuName,
                duongKinh: size.duongKinhCM,
                suon: size.suonNuong,
                salad: size.saladGr,
                loaiPizza: typePizza,
                idVourcher: form.idVourcher,
                idLoaiNuocUong: drink,
                soLuongNuoc: size.drink,
                hoTen: form.hoTen,
                thanhTien: size.priceVND,
                email: form.email,
                soDienThoai: form.soDienThoai,
                diaChi: form.diaChi,
                loiNhan: form.loiNhan
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };
        fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data);
                setMaDonHang(data.orderId)
                handleClose();
                setOpenCon(true)
            })
    }

    return (
        <Grid container mt={6}>
            {/* <!-- MODAL --> */}
            <Modal open={open} onClose={handleClose} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
                <Box sx={style}>
                    <Typography align="center" variant="h4" mb={2}>
                        Thông tin đơn hàng
                    </Typography>
                    <Typography>
                        Họ và tên: {form.hoTen}
                    </Typography>
                    <Typography>
                        Số điện thoại: {form.soDienThoai}
                    </Typography>
                    <Typography>
                        Địa chỉ: {form.diaChi}
                    </Typography>
                    <Typography>
                        Lời nhắn: {form.loiNhan}
                    </Typography>
                    <Typography>
                        Mã giảm giá: {form.idVourcher} (Giảm giá: {form.giamGia}%)
                    </Typography>
                    <Typography variant="h6" mt={2}>
                        Thông tin chi tiết:
                    </Typography>
                    <Typography>
                        Combo đã chọn: {size.menuName ? size.menuName : <Typography color="error" sx={{display:"inline"}}>Chưa chọn cỡ pizza</Typography>}
                    </Typography>
                    <Typography>
                        Loại nước uống: {drink ? drink : <Typography color="error" sx={{display:"inline"}}>Chưa chọn loại nước uống</Typography>}
                    </Typography>
                    <Typography mb={4}>
                        Loại pizza: {typePizza ? typePizza : <Typography color="error" sx={{display:"inline"}}>Chưa chọn loại pizza</Typography>}
                    </Typography>
                    {(size.menuName && drink && typePizza) ? <Button variant="contained" color="success" onClick={taoDonHang}>Tạo đơn</Button> : <Typography color="error" mb={2}>Chưa đủ thông tin để tạo đơn ( Nút tạo đơn sẽ hiện nếu bạn chọn đủ thông tin )</Typography>}
                    &nbsp;&nbsp;<Button variant="contained" color="error" onClick={handleClose}>Hủy bỏ</Button>
                </Box>
            </Modal>

            {/* <!-- MODAL SAU CONFIRM --> */}
            <Modal open={openCon} onClose={handleCloseCon} aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description">
                <Box sx={style}>
                    <Typography align="center" variant="h5">
                        Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là: {maDonHang}
                    </Typography>
                </Box>
            </Modal>

            <Grid id="container1">
                <Grid sm={12}>
                    <IntroduceComponent></IntroduceComponent>
                    <SizeComponent sizeProps={getSize}></SizeComponent>
                    <TypeComponent typeProps={getType}></TypeComponent>
                    <DrinkComponent drinkProps={getDrink}></DrinkComponent>
                    <FormComponent formProps={getForm}></FormComponent>
                </Grid>
            </Grid>
        </Grid>
    )

}
export default ContentComponent;