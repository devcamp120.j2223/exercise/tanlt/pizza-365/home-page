import { Container, Grid, Typography, Button} from '@mui/material';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebookSquare, faInstagram, faSnapchat, faPinterestP, faTwitter, faLinkedinIn } from "@fortawesome/free-brands-svg-icons"
import {faArrowUp} from "@fortawesome/free-solid-svg-icons"

function FooterComponent() {
        return (
            <Container sx={{width:"100%"}} className="bg-orange">
                <Grid textAlign="center" padding={5}>
                    <Grid sm={12}>
                        <Typography variant='h5' m={2}>Footer</Typography>
                        <Button variant='contained' sx={{ backgroundColor: "black", color: "white"}} m={3}><FontAwesomeIcon icon={faArrowUp} /> To the top</Button>
                        <Grid m={2}>
                            <FontAwesomeIcon icon={faFacebookSquare} />&nbsp;
                            <FontAwesomeIcon icon={faInstagram} />&nbsp;
                            <FontAwesomeIcon icon={faSnapchat} />&nbsp;
                            <FontAwesomeIcon icon={faPinterestP} />&nbsp;
                            <FontAwesomeIcon icon={faTwitter} />&nbsp;
                            <FontAwesomeIcon icon={faLinkedinIn} />&nbsp;
                            <Typography>Powered by DEVCAMP</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
        )
}
export default FooterComponent;